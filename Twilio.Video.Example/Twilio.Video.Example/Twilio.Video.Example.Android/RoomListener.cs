﻿using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tvi.Webrtc;
using Twilio.Exceptions;
using TwilioVideo;
using VideoView = TwilioVideo.VideoView;

namespace Twilio.Video.Example.Droid
{
    public class RoomListener : Java.Lang.Object, Room.IListener
    {
        const string TAG = "RoomListener";
        RemoteParticipant.IListener remoteParticipantListener;
        VideoView primaryVideoView;
        public RoomListener(RemoteParticipant.IListener remoteParticipantListener, VideoView primaryVideoView)
        {
            this.remoteParticipantListener = remoteParticipantListener;
            this.primaryVideoView = primaryVideoView;
        }
        //public IntPtr Handle => throw new NotImplementedException();

        //public int JniIdentityHashCode => throw new NotImplementedException();

        //public JniObjectReference PeerReference => throw new NotImplementedException();

        //public JniPeerMembers JniPeerMembers => throw new NotImplementedException();

        public JniManagedPeerStates JniManagedPeerState => throw new NotImplementedException();

        public void Disposed()
        {
            throw new NotImplementedException();
        }

        public void DisposeUnlessReferenced()
        {
            throw new NotImplementedException();
        }

        public void Finalized()
        {
            throw new NotImplementedException();
        }

        public void OnConnected(Room room)
        {
            LocalParticipant localParticipant = room.LocalParticipant;
            RemoteParticipant participant = room.RemoteParticipants.FirstOrDefault();
            if(null!= participant)
            {
                participant.SetListener(remoteParticipantListener);
            }

            SetSpeaker(true);
        }

        private void SetSpeaker(bool onState)
        {
            AudioManager audioManager = Android.App.Application.Context.GetSystemService(Context.AudioService) as AudioManager;
            if (null != audioManager)
            {
                audioManager.SpeakerphoneOn = onState;
            }
        }

        public void OnConnectFailure(Room p0, TwilioVideo.TwilioException p1)
        {
            Logging.D(TAG, $"Connect to {p0.Name} failed . Error {p1?.Message}");
            SetSpeaker(false);
        }

        public void OnDisconnected(Room p0, TwilioVideo.TwilioException p1)
        {
            Logging.D(TAG, $"Disconected from {p0.Name}. Error {p1?.Message}");
            SetSpeaker(false);
        }

        public void OnDominantSpeakerChanged(Room room, RemoteParticipant remoteParticipant)
        {
            Logging.D(TAG, $"Dominant Speaker Changed to {remoteParticipant.Identity} in {room.Name}");
        }

        public void OnParticipantConnected(Room p0, RemoteParticipant participant)
        {
            Logging.D(TAG, $"Participant {participant.Identity} Connected  in {p0.Name}");
            participant.SetListener(remoteParticipantListener);
        }

        public void OnParticipantDisconnected(Room p0, RemoteParticipant p1)
        {
            Logging.D(TAG, $"Participant {p1.Identity} disconnected  from {p0.Name}");
        }

        public void OnReconnected(Room p0)
        {
            Logging.D(TAG, $"Reconnected to {p0.Name}");
        }

        public void OnReconnecting(Room p0, TwilioVideo.TwilioException p1)
        {
            Logging.D(TAG, $"Reconnecting to {p0.Name} due to {p1.Message}");
        }

        public void OnRecordingStarted(Room p0)
        {
            Logging.D(TAG, $"Recording Started in {p0.Name}");
        }

        public void OnRecordingStopped(Room p0)
        {
            Logging.D(TAG, $"Recording Stopped in {p0.Name}");
        }

        public void SetJniIdentityHashCode(int value)
        {
            Logging.D(TAG, $"Set Jni Identity Hash Code {value}");            
        }

        public void SetJniManagedPeerState(JniManagedPeerStates value)
        {
            Logging.D(TAG, $"Set Jni Managed Peer State {value}");
        }

        public void SetPeerReference(JniObjectReference reference)
        {
            Logging.D(TAG, $"Set Peer Reference");
        }
    }
}