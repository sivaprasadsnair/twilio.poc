﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Twilio.Video.Example.Droid;
using Twilio.Video.Example.Services;
using Twilio.Video.Example.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VideoPage), typeof(VideoPageRenderer))]
namespace Twilio.Video.Example.Droid
{
    [Obsolete]
    public class VideoPageRenderer : PageRenderer, TextureView.ISurfaceTextureListener
    {
        VideoRenderer videoRenderer;
        Activity activity;
        global::Android.Views.View view;
        readonly Random random = new Random();
        public VideoPageRenderer()
        {
            videoRenderer = new VideoRenderer();
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            var msw = MeasureSpec.MakeMeasureSpec(r - l, MeasureSpecMode.Exactly);
            var msh = MeasureSpec.MakeMeasureSpec(b - t, MeasureSpecMode.Exactly);

            view.Measure(msw, msh);
            view.Layout(0, 0, r - l, b - t);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                SetupUserInterface();
                SetupEventHandlers();
                AddView(view);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"            ERROR: ", ex.Message);
            }
        }

        void SetupUserInterface()
        {
            activity = this.Context as Activity;
            view = activity.LayoutInflater.Inflate(Twilio.Video.Example.Droid.Resource.Layout.VideoViewLayout, this, false);

            var text = view.FindViewById<EditText>(Resource.Id.editText1);
            if (null != text)
            {
                text.Text = $"User {random.Next(1000)}";
            }
        }

        async void ConnectButtonTapped(object sender, EventArgs e)
        {
            var textureView = view.FindViewById<global::Android.Widget.Button>(Resource.Id.button1);
            if(textureView.Text == "CONNECT")
            {
                var videoView = view.FindViewById<TwilioVideo.VideoView>(Resource.Id.videoView1);
                var primaryView = view.FindViewById<TwilioVideo.VideoView>(Resource.Id.videoView2);
                var text = view.FindViewById<EditText>(Resource.Id.editText1);
                string userName = "example user";
                if (null != text)
                {
                    userName = text.Text;
                }

                ConnectParam connectParam = new ConnectParam()
                {
                    UserIdentity = userName,
                    LocalView = videoView,
                    PrimaryView = primaryView
                };
                videoRenderer.Start(connectParam);
                textureView.Text = "DISCONNECT";
            }
            else
            {
                videoRenderer.Stop();
                textureView.Text = "CONNECT";
            }
 
        }

        void SetupEventHandlers()
        {
            var textureView = view.FindViewById<global::Android.Widget.Button>(Resource.Id.button1);
            textureView.Text = "CONNECT";
            textureView.Click += ConnectButtonTapped;
        }
        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
        {
            throw new NotImplementedException();
        }

        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
        {
            throw new NotImplementedException();
        }

        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
        {
            throw new NotImplementedException();
        }

        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
        {
            throw new NotImplementedException();
        }
    }
}