﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Twilio.Video.Example.Droid
{
    public class ConnectParam
    {
        public TwilioVideo.VideoView LocalView { get; set; }
        public TwilioVideo.VideoView PrimaryView { get; set; }
        public string UserIdentity { get; set; }
    }
}