﻿using Android.Hardware.Camera2;
using Android.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Tvi.Webrtc;
using Twilio.Jwt.AccessToken;
using TwilioVideo;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.FastRenderers;
using Size = Xamarin.Forms.Size;
using VideoView = TwilioVideo.VideoView;

namespace Twilio.Video.Example.Droid
{
    public class VideoRenderer : VideoView, IVisualElementRenderer, IViewRenderer
    {
        RemoteParticipantListener remoteParticipantListener;
        Room room;
        LocalVideoTrack localVideoTrack = default;
        LocalAudioTrack localAudioTrack = default;
        VisualElementTracker visualElementTracker;
        VisualElementRenderer visualElementRenderer;
        public VideoRenderer()
            : base(Android.App.Application.Context)
        {
            //visualElementRenderer = new VisualElementRenderer(this);
            //visualElementTracker.

            View = this;
        }
        public VisualElement Element { get; set; }

        public VisualElementTracker Tracker { get; set; }

        public ViewGroup ViewGroup { get; set; }

        public Android.Views.View View { get; set; }

        public event EventHandler<VisualElementChangedEventArgs> ElementChanged;
        public event EventHandler<PropertyChangedEventArgs> ElementPropertyChanged;

        public SizeRequest GetDesiredSize(int widthConstraint, int heightConstraint)
        {
            return new SizeRequest()
            {
                Minimum = new Size(50, 50),
                Request = new Size(300, 300)
            };
        }

        public void MeasureExactly()
        {

        }

        public void SetElement(VisualElement element)
        {
        }

        public void SetLabelFor(int? id)
        {

        }

        public void UpdateLayout()
        {

        }

        public void Start(ConnectParam connectParams)
        {
            remoteParticipantListener = new RemoteParticipantListener(connectParams.PrimaryView);
            StartVideoCall(connectParams);
        }
        public void Stop()
        {
            if(null!= localAudioTrack)
            {
                //localAudioTrack.Release();
            }
            if(null!=localVideoTrack)
            {
                foreach(IVideoSink videoSink in localVideoTrack.Sinks)
                {
                    localVideoTrack.RemoveSink(videoSink);
                }

            }
            if(null!= room)
            {
                room.LocalParticipant.UnpublishTrack(localAudioTrack);
                room.LocalParticipant.UnpublishTrack(localVideoTrack);
                //localVideoTrack.Release();
                room.Disconnect();
            }            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                localVideoTrack?.Release();
                localAudioTrack?.Release();
            }
            base.Dispose(disposing);
        }
        private void StartVideoCall(ConnectParam connectParam)
        {
            CheckPermissions();
            string token = GenerateTwilioAccessToken(connectParam.UserIdentity);
            ConnectOptions connectOptions = new ConnectOptions.Builder(token)
                .RoomName("Room 1")
                .AudioTracks(GetAudioTracks())
                .VideoTracks(GetVideoTracks(connectParam.LocalView))
                .Build();
            var listener = new RoomListener(remoteParticipantListener, connectParam.PrimaryView);
            room = TwilioVideo.Video.Connect(Android.App.Application.Context, connectOptions, listener);
        }


        private void CheckPermissions()
        {
            try
            {
                var p = Permissions.CheckStatusAsync<Permissions.Camera>().Result;
                if (p == PermissionStatus.Denied)
                {
                    try
                    {
                        var status = Permissions.RequestAsync<Permissions.Camera>().Result;

                    }
                    catch (Exception ex)
                    {

                        throw;
                    }
                }
                p = Permissions.CheckStatusAsync<Permissions.Microphone>().Result;
                if (p == PermissionStatus.Denied)
                {
                    var status = Permissions.RequestAsync<Permissions.Microphone>().Result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private IList<LocalAudioTrack> GetAudioTracks()
        {
            localAudioTrack = LocalAudioTrack.Create(Android.App.Application.Context, true);
            return new List<LocalAudioTrack>() { localAudioTrack };
        }

        private IList<LocalVideoTrack> GetVideoTracks(TwilioVideo.VideoView videoView)
        {
            IList<LocalVideoTrack> localVideoTracks = new List<LocalVideoTrack>();

            // A video track requires an implementation of a VideoCapturer. Here's how to use the front camera with a Camera2Capturer.
            Camera2Enumerator camera2Enumerator = new Camera2Enumerator(Android.App.Application.Context);
            
            string frontCameraId = string.Empty;
            foreach (string cameraId in camera2Enumerator.GetDeviceNames())
            {
                if (camera2Enumerator.IsFrontFacing(cameraId))
                {
                    frontCameraId = cameraId;
                    break;
                }
            }
            if (!string.IsNullOrEmpty(frontCameraId))
            {
                Tvi.Webrtc.IVideoCapturer cameraCapturer = default;
                try//TODO:Remove the try catch later
                {
                    cameraCapturer = camera2Enumerator.CreateCapturer(frontCameraId, new CameraVideoCapturerCameraEventsHandler());
                    // Create a video track
                    localVideoTrack = LocalVideoTrack.Create(Android.App.Application.Context, true, cameraCapturer);
                }
                catch (Exception ex)
                {

                    throw;
                }
                // Rendering a local video track requires an implementation of VideoSink
                // Let's assume we have added a VideoView in our view hierarchy

                // Render a local video track to preview your camera
                localVideoTrack.AddSink(videoView);

                localVideoTracks.Add(localVideoTrack);
            }
            return localVideoTracks;
        }

        private string GenerateTwilioAccessToken(string identity)
        {
            var accountSid = "ACb5264c8ce65d7c66572664720061a468";
            var apiKeySid = "SK40afff55596df74226c34e5ac169015e";
            var apiKeySecret = "UhQrrAXLauJAR0Asnyv8jvAQ98jHjR48";

            //var identity = "example-user";

            // Create a video grant for the token
            var grant = new VideoGrant();
            grant.Room = "Room 1";
            var grants = new HashSet<IGrant> { grant };

            // Create an Access Token generator
            var token = new Token(accountSid, apiKeySid, apiKeySecret, identity: identity, grants: grants);

            // Serialize the token as a JWT
            return token.ToJwt();
        }
    }

    
}