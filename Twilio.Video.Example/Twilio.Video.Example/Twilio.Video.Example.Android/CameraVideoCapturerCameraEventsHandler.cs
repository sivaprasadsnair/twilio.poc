﻿using Tvi.Webrtc;

namespace Twilio.Video.Example.Droid
{
    public class CameraVideoCapturerCameraEventsHandler : Java.Lang.Object, ICameraVideoCapturerCameraEventsHandler
    {
        const string TAG = "CameraVideoCapturerCameraEventsHandler";
        public void OnCameraClosed()
        {
            Logging.D(tag: TAG, $"Camera is closed");
        }

        public void OnCameraDisconnected()
        {
            Logging.D(tag: TAG, $"Camera is disconnected");
        }

        public void OnCameraError(string p0)
        {
            Logging.D(tag: TAG, $"Camera {p0} is error");
        }

        public void OnCameraFreezed(string p0)
        {
            Logging.D(tag: TAG, $"Camera {p0} is frezed");
        }

        public void OnCameraOpening(string p0)
        {
            Logging.D(tag: TAG, $"Camera {p0} is opening");
        }

        public void OnFirstFrameAvailable()
        {
            Logging.D(tag: TAG, $"Camera first frame available");
        }
    }
}