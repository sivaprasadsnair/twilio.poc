﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tvi.Webrtc;
using TwilioVideo;

namespace Twilio.Video.Example.Droid
{
    public class RemoteParticipantListener : Java.Lang.Object, RemoteParticipant.IListener
    {
        TwilioVideo.VideoView primaryVideoView;
        const string TAG = "RemoteParticipantListener";

        public RemoteParticipantListener(TwilioVideo.VideoView primaryVideoView)
        {
            this.primaryVideoView = primaryVideoView;
        }
        public void OnAudioTrackDisabled(RemoteParticipant p0, RemoteAudioTrackPublication p1)
        {
            Logging.D(TAG, $"Audio track disabled by {p0.Identity}");
        }

        public void OnAudioTrackEnabled(RemoteParticipant p0, RemoteAudioTrackPublication p1)
        {
            Logging.D(TAG, $"Audio track enabled by {p0.Identity}");
        }

        public void OnAudioTrackPublished(RemoteParticipant p0, RemoteAudioTrackPublication p1)
        {
            Logging.D(TAG, $"Audio track published by {p0.Identity}");
        }

        public void OnAudioTrackSubscribed(RemoteParticipant p0, RemoteAudioTrackPublication p1, RemoteAudioTrack p2)
        {
            Logging.D(TAG, $"Audio track subscribed by {p0.Identity}");
            //p2.AddSink()
        }

        public void OnAudioTrackSubscriptionFailed(RemoteParticipant p0, RemoteAudioTrackPublication p1, TwilioException p2)
        {
            Logging.D(TAG, $"Audio track subscription failed by {p0.Identity}");
        }

        public void OnAudioTrackUnpublished(RemoteParticipant p0, RemoteAudioTrackPublication p1)
        {
            Logging.D(TAG, $"Audio track unpublished by {p0.Identity}");
        }

        public void OnAudioTrackUnsubscribed(RemoteParticipant p0, RemoteAudioTrackPublication p1, RemoteAudioTrack p2)
        {
            Logging.D(TAG, $"Audio track unsubscribed by {p0.Identity}");
        }

        public void OnDataTrackPublished(RemoteParticipant p0, RemoteDataTrackPublication p1)
        {
            Logging.D(TAG, $"Data track published by {p0.Identity}");
        }

        public void OnDataTrackSubscribed(RemoteParticipant p0, RemoteDataTrackPublication p1, RemoteDataTrack p2)
        {
            Logging.D(TAG, $"Data track subscribed by {p0.Identity}");
        }

        public void OnDataTrackSubscriptionFailed(RemoteParticipant p0, RemoteDataTrackPublication p1, TwilioException p2)
        {
            Logging.D(TAG, $"Data track subscription failed by {p0.Identity}");
        }

        public void OnDataTrackUnpublished(RemoteParticipant p0, RemoteDataTrackPublication p1)
        {
            Logging.D(TAG, $"Data track unpublished by {p0.Identity}");
        }

        public void OnDataTrackUnsubscribed(RemoteParticipant p0, RemoteDataTrackPublication p1, RemoteDataTrack p2)
        {
            Logging.D(TAG, $"Data track unsubscribed by {p0.Identity}");
        }

        public void OnNetworkQualityLevelChanged(RemoteParticipant remoteParticipant, NetworkQualityLevel networkQualityLevel)
        {
            Logging.D(TAG, $"Data track network quality level changed by {remoteParticipant.Identity}");
        }

        public void OnVideoTrackDisabled(RemoteParticipant p0, RemoteVideoTrackPublication p1)
        {
            Logging.D(TAG, $"Video track disabled by {p0.Identity}");
        }

        public void OnVideoTrackEnabled(RemoteParticipant p0, RemoteVideoTrackPublication p1)
        {
            Logging.D(TAG, $"Video track enabled by {p0.Identity}");
        }

        public void OnVideoTrackPublished(RemoteParticipant p0, RemoteVideoTrackPublication p1)
        {
            Logging.D(TAG, $"Video track published by {p0.Identity}");
        }

        public void OnVideoTrackSubscribed(RemoteParticipant p0, RemoteVideoTrackPublication p1, RemoteVideoTrack remoteVideoTrack)
        {
            Logging.D(TAG, $"Video track subscribed by {p0.Identity}");
            primaryVideoView.SetMirror(false);
            remoteVideoTrack.AddSink(primaryVideoView);
        }

        public void OnVideoTrackSubscriptionFailed(RemoteParticipant p0, RemoteVideoTrackPublication p1, TwilioException p2)
        {
            Logging.D(TAG, $"Video track subscription failed by {p0.Identity}");
        }

        public void OnVideoTrackUnpublished(RemoteParticipant p0, RemoteVideoTrackPublication p1)
        {
            Logging.D(TAG, $"Video track unpublished by {p0.Identity}");
        }

        public void OnVideoTrackUnsubscribed(RemoteParticipant p0, RemoteVideoTrackPublication p1, RemoteVideoTrack p2)
        {
            Logging.D(TAG, $"Video track unsubscribed by {p0.Identity}");
        }
    }
}