﻿using System.ComponentModel;
using Twilio.Video.Example.ViewModels;
using Xamarin.Forms;

namespace Twilio.Video.Example.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}