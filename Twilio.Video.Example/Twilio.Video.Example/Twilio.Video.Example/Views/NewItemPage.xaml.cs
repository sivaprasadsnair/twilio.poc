﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Twilio.Video.Example.Models;
using Twilio.Video.Example.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Twilio.Video.Example.Views
{
    public partial class NewItemPage : ContentPage
    {
        public Item Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();
            BindingContext = new NewItemViewModel();
        }
    }
}