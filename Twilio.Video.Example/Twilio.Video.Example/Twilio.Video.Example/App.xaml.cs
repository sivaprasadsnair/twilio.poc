﻿using System;
using Twilio.Video.Example.Services;
using Twilio.Video.Example.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Twilio.Video.Example
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
