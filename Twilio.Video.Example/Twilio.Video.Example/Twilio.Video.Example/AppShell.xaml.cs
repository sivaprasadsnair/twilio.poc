﻿using System;
using System.Collections.Generic;
using Twilio.Video.Example.ViewModels;
using Twilio.Video.Example.Views;
using Xamarin.Forms;

namespace Twilio.Video.Example
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
